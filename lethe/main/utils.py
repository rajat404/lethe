import phonenumbers
from phonenumbers import timezone
from settings import client, lookupclient


def check_number(number_entered):
    """
    Checks whether the number entered is valid or not
    Also return the list of timezones, the number belongs to
    """
    try:
        lookup_object = lookupclient.phone_numbers.get(number_entered)
        temp_number = phonenumbers.parse(number_entered, None)
        return (lookup_object,
                str(timezone.time_zones_for_number(temp_number)))
    except Exception as e:
        return False, e.message


def validate_new_number(new_number, new_name='new_user'):
    """
    Validates a number on twilio, coz messages can be sent
    by a trial account only to numbers which are validated
    """
    # Change the delay value later on
    response = client.caller_ids.validate(
        phone_number=new_number, friendly_name=new_name, call_delay=10)
    return response
