from lethe import app
# Wrapper for DB
from peewee import *
from playhouse.sqlite_ext import *
from playhouse.flask_utils import FlaskDB
import datetime

flask_db = FlaskDB(app)
db_instance = flask_db.database


class BaseModel(flask_db.Model):

    class Meta:
        database = db_instance


class Person(BaseModel):
    mob_number = CharField(unique=True)
    country_code = CharField()
    pname = CharField()
    is_verified = BooleanField(default=False)
    created_at = DateTimeField(default=datetime.datetime.utcnow())
    timezones = CharField()


class MessageLog(BaseModel):
    mobile = ForeignKeyField(Person, related_name='logs')
    timestamp = DateTimeField(default=datetime.datetime.utcnow())
    is_delivered = BooleanField(default=False)
