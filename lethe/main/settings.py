import os
from ConfigParser import SafeConfigParser
from twilio.rest import TwilioRestClient, TwilioLookupsClient

APP_DIR = os.path.dirname(__file__)
CONFIG_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
DATABASE = 'sqliteext:///%s' % os.path.join(APP_DIR, 'people.db')

# Read credentials from the config file
config_reader = SafeConfigParser()
config_reader.read(CONFIG_DIR+'/config.ini')
account = config_reader.get("credentials", "account")
token = config_reader.get("credentials", "token")
global_sender = config_reader.get("credentials", "sender")
client = TwilioRestClient(account, token)
lookupclient = TwilioLookupsClient(account, token)
