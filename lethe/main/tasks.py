from settings import client, global_sender
from models import Person, MessageLog

import arrow
import datetime
import time


def send_message(recipient, sender, message):
    """
    Send SMS to the specified number
    """
    try:
        message = client.messages.create(
            to=recipient, from_=sender, body=message)
        success = True
    except Exception as e:
        message = e
        success = False
    return success, message


def status_checker(message_success_obj):
    """
    Checks whether the SMS was delivered or not
    """
    current_status = client.messages.get(message_success_obj.name)
    msg_status = False if current_status.status == 'failed' or \
        current_status.status == 'undelivered' else True
    return msg_status


def cleanify(all_zones):
    """
    Cleans the `timezone` value for use
    """
    extras = ['(', ')', ',', '\'']
    clean = ''
    for zone in all_zones:
        if zone not in extras:
            clean = clean + zone
    return clean


def time_in_range(start, end, x):
    """
    Return true if x is in the range [start, end]
    """
    if start <= end:
        return start <= x <= end
    else:
        return start <= x or x <= end


def get_all_users():
    """
    Retrieve information of all users from the DB
    """
    for user in Person.select():
        judge(user._data)


def judge(user_data):
    """
    Calculates the time according to the User's timezone
    and if it lies in daytime, then send SMS
    """
    seven_am = datetime.time(7, 0, 0)
    eleven_pm = datetime.time(23, 0, 0)
    clean_zone = cleanify(user_data['timezones'])
    utc = arrow.utcnow()
    local = utc.to(clean_zone)
    temp_time = local.time()
    should_send_msg = time_in_range(seven_am, eleven_pm, temp_time)
    if should_send_msg:
        lawful_spammer(user_data)


def lawful_spammer(user_data, fail_count=0):
    """
    Sends the Users a SMS telling them, their name
    """
    user_message = "Hi! Your name is "+user_data['pname'] +\
        ". U suffer from amnesia and will forget" +\
        " everything after 1 hr. Will remind u after 1 hr"
    user_obj = Person.get(Person.mob_number == user_data['mob_number'])
    while(fail_count < 6):
        message_success, message_obj = send_message(
            user_data['mob_number'], global_sender, user_message)
        if message_success == False:
            # unvalidated number
            MessageLog.create(mobile=user_obj, is_delivered=False)
            return False
        time.sleep(7)
        is_delivered = status_checker(message_obj)
        MessageLog.create(mobile=user_obj, is_delivered=is_delivered)
        if is_delivered == False:
            fail_count += 1
        else:
            return True
    return is_delivered

if __name__ == '__main__':
    get_all_users()
