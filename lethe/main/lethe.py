from flask import Flask, request
from flask_restful import Resource, Api
from playhouse.flask_utils import get_object_or_404
from settings import DATABASE, CONFIG_DIR
import datetime
import arrow

app = Flask(__name__)
app.config.from_object(__name__)
api = Api(app)

# Needs to be imported after creating the `app` instance
# since `models.py` imports `app`
from models import *
from utils import check_number, validate_new_number

# to log time of the app
with open(CONFIG_DIR+'/uptime.txt', "a") as f:
    f.write(str(datetime.datetime.utcnow())+'\n')


class UserView(Resource):

    def get(self, mob_number):
        """
        Return User info from DB, if it exists
        """
        user_obj = get_object_or_404(Person, Person.mob_number == mob_number)
        response = user_obj._data
        response['created_at'] = str(response['created_at'])
        return {'status': 'success', 'data': response}, 200

    def post(self, mob_number):
        """
        Inserts the information of the newly registered user
        """
        req = request.get_json(force=True)
        user_number = req['mob_number']
        validity, info = check_number(user_number)
        try:
            if validity:
                person, created = Person.get_or_create(
                    pname=req['pname'],
                    mob_number=req['mob_number'],
                    country_code=validity.country_code,
                    timezones=info)
                if created:
                    # Uncomment later, to avoid calls while testing
                    # Don't forget the obj in return statement as well

                    validation_obj = validate_new_number(
                        user_number, req['pname'])
                    return {'status': 'success', 'data': validation_obj}, 201
                else:
                    info = "user already exists"
            else:
                info = "invalid number"
        except Exception as e:
            info = e.message
        return {'status': 'failed', 'error': info}, 400

    def delete(self, mob_number):
        """
        Delete a user in case s/he wants to unsubscribe
        """
        user_obj = get_object_or_404(Person, Person.mob_number == mob_number)
        user_obj.delete_instance()
        return {'status': 'success',
                'data': "successfully deleted this user"}, 200

    def patch(self, mob_number):
        """
        Update the `is_verified` flag for a user
        """
        user_obj = get_object_or_404(Person, Person.mob_number == mob_number)
        user_obj.is_verified = True
        user_obj.save()
        return {'status': 'success',
                'data': "successfully updated the object"}, 200


class LogView(Resource):

    def get(self, mob_number):
        """
        Show failed logs for each User
        """
        all_logs = []
        for log in MessageLog.select().where(mob_number == mob_number):
            human = arrow.get(log.timestamp)
            log.timestamp = str(log.timestamp)
            log._data['human_time'] = human.humanize()
            all_logs.append(log._data)
        response = all_logs
        return {'status': 'success', 'data': response}, 200


class UptimeView(Resource):

    def get(self):
        """
        Show time since app was up. So in event the app was restarted,
        the latest time would show up.
        """
        with open('uptime.txt', "r") as f:
            w = f.readlines()
        response = {}
        response['time'] = w[-1].strip()
        human = arrow.get(response['time'])
        response['human_time'] = human.humanize()
        return {'status': 'success', 'data': response}, 200


api.add_resource(UserView, '/api/user/<mob_number>/')
api.add_resource(LogView, '/api/logs/<mob_number>/')
api.add_resource(UptimeView, '/api/uptime/')
