from main.lethe import app
from main.models import db_instance, Person, MessageLog
from werkzeug.serving import run_simple

if __name__ == '__main__':
    db_instance.create_tables([Person,MessageLog], safe=True)
    run_simple('0.0.0.0', 9200, app, use_reloader=True, use_debugger=False)
