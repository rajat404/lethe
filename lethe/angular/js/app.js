angular.module('letheApp',['ngRoute', 'mainController', 'mainService'])
.config(['$routeProvider', '$locationProvider', function($routeProvider) {
  $routeProvider.
        when("/", {templateUrl: "views/signup.html"}).
        when("/signup", {templateUrl: "views/signup.html", controller: "signupController"}).
        when("/signin", {templateUrl: "views/signin.html", controller: "logController"}).
        otherwise({redirectTo: '/'});
}]);
