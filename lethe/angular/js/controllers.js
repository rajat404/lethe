angular.module('mainController', [])

    .controller('logController', ['$scope', '$http', 'logService', function($scope, $http, logService) {

        $scope.getLogs = function(){
            var temp_number = String('+'+$scope.country_code+$scope.mob_number);

            logService.get(temp_number).success(function(data) {
                $scope.response = data
                console.log($scope.response);
            });

            $http.get('/api/uptime/', {cache: false}).success(function(data){
                $scope.total_uptime = data['data']['time'];
                $scope.total_human_uptime = data['data']['human_time'];
            });

        }

    }])

    .controller('signupController', ['$scope','$http', 'signupService', function($scope, $http, signupService) {

        $scope.signup = function(){
            var temp_number = String('+'+$scope.country_code+$scope.mob_number);
            var userinfo = {'pname':$scope.pname,'mob_number':temp_number};

            signupService.post(userinfo).success(function(data) {
                $scope.token = data['data']['validation_code'];
            });
        }

    }]);
