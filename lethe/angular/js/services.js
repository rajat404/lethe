angular.module('mainService', [])

    .factory('logService', ['$http',function($http) {
        return {
            get : function(userinfo) {
                url = String('/api/logs/'+userinfo+'/');
                return $http.get(url);
            }
        }
    }])

    .factory('signupService', ['$http',function($http) {
        return {
            post : function(userinfo) {
                url = String('/api/user/'+userinfo['mob_number']+'/');
                return $http.post(url, userinfo);
            }
        }
    }]);
