Lethe
======    

The app every amnesiac is waiting for...until s/he forgets about it :P


Setting Up the project Locally
-------------------------------

* Clone the repo from [here](https://gitlab.com/rajat404/lethe)
* Copy the path to the project directory and paste it at the specified places in `sample.nginx` file, in the deployment directory. Then rename the file to `nginx.conf` or the likes. Copy this file (or create sym link) to your system's nginx `sites-enabled` directory, and restart nginx.
* Rename `sample.config.ini` to `config.ini` and replace the fake credentials with your API key & secret.
* Create a virtualenv and activate it (preferable method), by running:  

```
virtualenv venv
source venv/bin/activate
```
    
    
`Note:` Since I'm using the trail version, and not the corporate version like you guys, there might be some unforseen issues, which might mess up the code. So as a contingency, I've added the Database: `people.db` in the repository itself. It's unorthodox (and probably won't be needed), but it's only to be used in event of some major malfunction.    
    



* Run `pip install -r requirements.txt`
* That should do it. Run `python runserver.py`       
      
Now the APIs are running at localhost:9200, and the UI is running at localhost:9000     
      
* Open a new tab in Chrome and head over to `localhost:9000`.
* You should be at the registration page. Enter your number to enroll, and enjoy the reminders.   
     

Setting Up the Cron Job
-----------------------  

Open `crontab_config.txt` in the deployment directory and follow the instructions.        

      

Now all the people who have registered on the app shall receive SMSs at every hour.  

(In case of any trouble in setup, or for further clarification, please drop me a mail.)  

The tech stack & reasons for choosing it
----------------------------------------  

Since it was a small app, which according to the requirements is supposed to function for only 1 user, I tried to use the most basic tech, and avoided unnecessary overhead.  

* Flask-Restful: Small, simple framework built for making RESTful APIs.
* SQLite: No need to use MySQL, since we are going to store only a handful of records.
* Peewee: A small ORM which supports a variety of Databases.
* Cron: The app needs to support only a single user, and not on a dynamic timing, thus no need to use Celery, and a message broker along with it. Cron is good enough to schedule the task.
* AngularJS: AngularJS is the only frontend framework I know. I'd say that's good enough reason.
* Bootstrap: I don't know CSS, and Bootstrap is the best option to make a non-ugly UI.    


Having said that, had it been a huge, sophisticated app with scalability needs, I probably would have considered making a Django app, which uses Celery for scheduling, along with RabbitMQ as message broker, and MySQL for DB.   

